// 菜单配置
// headerMenuConfig：头部导航配置

const headerMenuConfig = [
  {
    name: '首页',
    path: '/',
    icon: 'home',
  },
  {
    name: '反馈',
    path: 'https://github.com/alibaba/ice',
    external: true,
    newWindow: true,
    icon: 'message',
  },
  {
    name: '帮助',
    path: 'https://alibaba.github.io/ice',
    external: true,
    newWindow: true,
    icon: 'bangzhu',
  },
];

// asideMenuConfig：侧边导航配置

const asideMenuConfig = [
  {
    name: 'Dashboard',
    path: '/',
    icon: 'home',
  },
  {
    name: '客户信息',
    path: '/user',
    icon: 'yonghu',
    children: [
      {
        name: '用户列表',
        path: '/user/userList',
      },
    ],
  },
  {
    name: '设备信息',
    path: '/device',
    icon: 'pin',
    children: [
      {
        name: '设备列表',
        path: '/device/deviceList',
      },
    ],
  },
  {
    name: '地图',
    path: '/mapp',
    icon: 'cascades',
  },
  {
    name: '通用设置',
    path: '/setting',
    icon: 'shezhi',
    children: [
      {
        name: '基础设置',
        path: '/setting/basic',
      },
      {
        name: '菜单设置',
        path: '/setting/navigation',
      },
    ],
  },
  {
    name: 'LoginPage',
    path: '/LoginPage',
    icon: 'home',
  },
];

export { headerMenuConfig, asideMenuConfig };
