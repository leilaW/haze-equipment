

import React, { Component } from 'react';
import StatisticalCard from './components/StatisticalCard';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import TabTable from './components/TabTable';
import { Map } from 'react-amap';

import { Grid, Button, Notice, Icon, Card, Tab, Tag, Table, Switch } from "@icedesign/base";
// import '@icedesign/data-binder';
const { Row, Col } = Grid;
//tab选项卡
const TabPane = Tab.TabPane;

const user = {
  name: 'sun',
  phone: '18638729321',
  address: '河南省郑州市金水区',
  uid: '1001',
  time1: '2018-05-06',
};

function handleChange(key) {
  console.log("change", key);
}

function handleClick(key) {
  console.log("click", key);
}

// config props
const visible = true; 
const radius = 30; 
const zooms = [3, 18];
const dataSet = {
  data: '',
  max: 100
}


//
export default class UserList extends Component {
  static displayName = 'UserDetail';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '用户管理', link: '' },
      { text: '用户列表', link: '#/user/userList' },
      { text: '用户详情', link: '#/user/userDetial' },
    ];

    const styles = {
      basicDetailTitle: {
        margin: '10px 0',
        fontSize: '16px',
      },
      infoColumn: {
        marginLeft: '16px',
      },
      infoColumnTitle: {
        margin: '20px 0',
        paddingLeft: '10px',
        borderLeft: '3px solid #3080fe',
        fontSize: 20
      },
      infoItems: {
        padding: 0,
        marginLeft: '80px',
      },
      infoItem: {
        marginBottom: '18px',
        listStyle: 'none',
        fontSize: '14px',
      },
      infoItemLabel: {
        minWidth: '70px',
        color: '#000',
        fontSize: 17,
      },
      infoItemValue: {
        color: '#333',
      },
      attachLabel: {
        minWidth: '70px',
        color: '#999',
        float: 'left',
      },
      attachPics: {
        width: '80px',
        height: '80px',
        border: '1px solid #eee',
        marginRight: '10px',
      },
      bigNum: {
        color: 'rgb(141,141,141)',
        // fontSize: 13,
      },
      symbol: {
        color: '#333',
        fontSize: 30,
        marginLeft: 10,
      },
      userMap: {
        marginBottom:'20px',
        border: '1px solid',
        height: 550,
        background: '#fff', borderRadius: '8px'
      }

    };

    return (
      <div className="create-user-page">
        <CustomBreadcrumb dataSource={breadcrumb} />

        <Card style={{ width: '100%', borderRadius: '6px', marginBottom: '20px' }} bodyHeight='auto'>
          <Row><h2 style={{ color: '#000', paddingLeft: 15, fontWeight: 700, fontSize: 22, marginBottom: 15, marginTop: 0 }}> <img width={30} src="http://112.74.168.74:8889/images/spec-detail-total/u2237.png" alt="" />  雾霾检测设备 </h2></Row>
          <div style={styles.infoColumn}>
            <Row wrap style={styles.infoItems}>
              <Col xxs="24" l="12" style={styles.infoItem}>
                <span style={styles.infoItemLabel}>用户姓名：</span>
                <span style={styles.infoItemValue}>{user.name}</span>
              </Col>
              <Col xxs="24" l="12" style={styles.infoItem}>
                <span style={styles.infoItemLabel}>用户编号：</span>
                <span style={styles.infoItemValue}>{user.uid}</span>
              </Col>
              <Col xxs="24" l="12" style={styles.infoItem}>
                <span style={styles.infoItemLabel}>联系电话：</span>
                <span style={styles.infoItemValue}>{user.phone} </span>
              </Col>
              <Col xxs="24" l="12" style={styles.infoItem}>
                <span style={styles.infoItemLabel}>联系地址：</span>
                <span style={styles.infoItemValue}>{user.address}</span>
              </Col>
            </Row>
          </div>
        </Card>

        <StatisticalCard />

        <TabTable />
        
        <div style={styles.userMap}>
          <Map amapkey={'eeb54f1e1bac5e5237b307344ad8e268'} version={'1.4.0'}>
            {/* <Heatmap {...pluginProps} /> */}
          </Map>
        </div>

        


      </div>
    );
  }
}
