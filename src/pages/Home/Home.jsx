import React, { Component } from 'react';

import RealTimeStatistics from './components/RealTimeStatistics';
import Tabchart from './components/TabChart';
import FeedList from './components/FeedList';
import ReviewDetailInfo from './components/ReviewDetailInfo';
import { Grid } from "@icedesign/base";
import ChartTypeLine from './components/ChartTypeLine';

const { Row, Col } = Grid;

export default class Home extends Component {
  static displayName = 'Home';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="home-page">

        <RealTimeStatistics />
  
        <Row gutter="20">
          <Col ><ChartTypeLine /></Col>
          <Col fixedSpan="16"><FeedList /></Col>
        </Row>

        <ReviewDetailInfo />

      </div>
    );
  }
}
