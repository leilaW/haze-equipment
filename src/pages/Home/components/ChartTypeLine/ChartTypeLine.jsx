import React, { Component } from 'react';
import { Chart, Axis, Geom, Tooltip } from 'bizcharts';
import { DataSet } from '@antv/data-set';
import IceContainer from '@icedesign/container';

export default class ChartTypeLine extends Component {
  static displayName = 'ChartTypeLine';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // 参考：https://alibaba.github.io/BizCharts/
    // 数据源
    const data = [
      { day: '17日', ZhengZhou: 133 },
      { day: '18日', ZhengZhou: 143 },
      { day: '19日', ZhengZhou: 159},
      { day: '20日', ZhengZhou: 165},
      { day: '21日', ZhengZhou: 89},
      { day: '22日', ZhengZhou: 38 },
      { day: '23日', ZhengZhou: 55 },
      { day: '24日', ZhengZhou: 65},
      { day: '25日', ZhengZhou: 87 },
      { day: '26日', ZhengZhou: 120 },
      { day: '27日', ZhengZhou: 110 },
      { day: '29日', ZhengZhou: 204},
      { day: '30日', ZhengZhou: 69},
      { day: '01日', ZhengZhou: 117},
      { day: '02日', ZhengZhou: 51},
      { day: '03日', ZhengZhou: 58},
      { day: '04日', ZhengZhou: 112},
      { day: '05日', ZhengZhou: 151},
      { day: '06日', ZhengZhou: 120},
      { day: '07日', ZhengZhou: 104},
      { day: '08日', ZhengZhou: 91},
    ];

    // DataSet https://github.com/alibaba/BizCharts/blob/master/doc/tutorial/dataset.md#dataset
    const ds = new DataSet();
    const dv = ds.createView().source(data);
    dv.transform({
      type: 'fold',
      fields: ['ZhengZhou'],
      key: 'city',
      value: 'AQI',
    });

    // 定义度量
    const cols = {
      month: {
        range: [0, 1],
      },
    };

    return (
      <div className="chart-type-line">
        <IceContainer>
          <h4 style={styles.title}>过去21天郑州空气质量指数（AQI）</h4>
          <Chart height={300} data={dv} scale={cols} forceFit>
            <Axis name="day" />
            <Axis
              name="AQI"
              label={{ formatter: (val) => `${val}` }}
            />
            <Tooltip crosshairs={{ type: 'y' }} />
            <Geom
              type="line"
              position="day*AQI"
              size={2}
              color="city"
              shape="smooth"
            />
            <Geom
              type="point"
              position="day*AQI"
              size={4}
              shape="circle"
              color="city"
              style={{ stroke: '#fff', lineWidth: 1 }}
            />
          </Chart>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  title: {
    margin: '0 0 40px',
    fontSize: '18px',
    paddingBottom: '15px',
    fontWeight: 'bold',
    borderBottom: '1px solid #eee',
    font: '#eee'
  },
};
