import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Chart, Axis, Geom, Tooltip, Legend } from 'bizcharts';
import { DataSet } from '@antv/data-set';


export default class TabChart extends Component {
  static displayName = 'TabChart';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const data = [
      {
        name: 'SaleNumber',
        'Jan.': 62,
        'Feb.': 65,
        'Mar.': 40,
        'Apr.': 24,
        May: 12,
        'Jun.': 14,
        'Jul.': 24,
        'Aug.': 22,
      },
    ];

    const ds = new DataSet();
    const dv = ds.createView().source(data);
    dv.transform({
      type: 'fold',
      fields: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.', 'Aug.'], // 展开字段集
      key: '月份', // key字段
      value: '平均月销售量', // value字段
    });

    return (
      <div className="chart-bar">
        <IceContainer>
          <Chart height={250} data={dv} forceFit>
            <Axis name="月份" />
            <Axis name="平均月销售量" />
            <Legend />
            <Tooltip crosshairs={{ type: 'y' }} />
            <Geom
              type="interval"
              position="月份*平均月销售量"
              color="name"
              adjust={[{ type: 'dodge', marginRatio: 1 / 32 }]}
            />
          </Chart>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  title: {
    margin: '0 0 40px',
    fontSize: '18px',
    paddingBottom: '15px',
    fontWeight: 'bold',
    borderBottom: '1px solid #eee',
  },
};
