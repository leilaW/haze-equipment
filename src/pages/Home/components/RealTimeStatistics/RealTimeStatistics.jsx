import React, { Component } from 'react';
import { Grid } from '@icedesign/base';

const { Row, Col } = Grid;

export default class RealTimeStatistics extends Component {
  static displayName = 'RealTimeStatistics';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Row  gutter="20" justify="center" >  
        <Col span='5'>
          <div style={{ ...styles.itemBody, ...styles.green ,paddingLeft:'30px '}}>
            <div style={styles.itemTitle}>
              <p style={styles.titleText}>用户总量</p>
            </div>
            <div style={styles.itemContent}>
              <h2 style={styles.itemNum}>8</h2>

                <p style={styles.desc}>当前用户总数量</p>

            </div>
          </div>
        </Col>
        <Col span='5'>
          <div style={{ ...styles.itemBody, ...styles.lightBlue,paddingLeft:'30px ' }}>
            <div style={styles.itemTitle}>
              <p style={styles.titleText}>设备总数</p>
            </div>
            <div style={styles.itemContent}>
              <h2 style={styles.itemNum}>10</h2>

                <p style={styles.desc}>当前监控设备数量</p>

            </div>
          </div>
        </Col>
        <Col span='5'>
          <div style={{ ...styles.itemBody, ...styles.darkBlue ,paddingLeft:'30px '}}>
            <div style={styles.itemTitle}>
              <p style={styles.titleText}>当前设备版本</p>
              {/* <span style={styles.tag}>实时</span> */}
            </div>
            <div style={styles.itemRow}>
              <div style={styles.itemCol}>
                <h2 style={styles.itemNum}>V1.0.0</h2>
                <p style={styles.desc}>版本号</p>
              </div>
            </div>
          </div>
        </Col>
        <Col span='5'>
          <div style={{ ...styles.itemBody, ...styles.navyBlue ,paddingLeft:'30px '}}>
            <div style={styles.itemTitle}>
              <p style={styles.titleText}>数据上传</p>
            </div>
            <div style={styles.itemRow}>
              <div style={styles.itemCol}>
                <h2 style={styles.itemNum}>508次</h2>
                <p style={styles.desc}>上传数据次数</p>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    );
  }
}

const styles = {
  item: {
    width: '25%',
    padding: '0 10px',
  },
  itemBody: {
    marginBottom: '20px',
    padding: '10px 20px',
    borderRadius: '4px',
    color: '#fff',
    height: '130px',
  },
  itemRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemTitle: {
    position: 'relative',
  },
  titleText: {
    margin: 0,
    fontSize: '18px',
  },
  tag: {
    position: 'absolute',
    right: 0,
    top: 0,
    padding: '2px 4px',
    borderRadius: '4px',
    fontSize: '12px',
    background: 'rgba(255, 255, 255, 0.3)',
  },
  itemNum: {
    margin: '16px 0',
    fontSize: '32px',
  },
  total: {
    margin: 0,
    fontSize: '14px',
  },
  desc: {
    margin: 0,
    fontSize: '12px',
  },
  green: {
    background: '#31B48D',
  },
  lightBlue: {
    background: '#38A1F2',
  },
  darkBlue: {
    background: '#7538C7',
  },
  navyBlue: {
    background: '#3B67A4',
  },
};
