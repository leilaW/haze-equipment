import React, { Component } from 'react';
// import Heatmap from 'react-amap-plugin-heatmap';
import iconTest from '../../../src/iconTest.jpg'
// import Map from 'react-amap/lib/map';
import { Map, InfoWindow } from 'react-amap';
import Marker from 'react-amap/lib/marker';

const Loading = <div style={loadingStyle}>Loading Map...</div>

const loadingStyle = {
  position: 'relative',
  height: '100%',
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
}



export default class Mapp extends Component {
  static displayName = 'Mapp';

  constructor(props) {
    super(props);
    this.state = {
      curVisibleWindow: null,
      count: 1,
      
    };

    this.position1 = 
    {
      longitude: 113.72,
      latitude: 34.75
    };
    this.position2={
      longitude: 113.69,
      latitude: 34.77
    }
    this.position3={
      longitude: 113.75,
      latitude: 34.567
    }
    this.position4={
      longitude: 113.722,
      latitude: 34.02
    }
    this.position5={
      longitude: 113.723,
      latitude: 34.8
    }


    

    this.markerEvents1 = {
      click: (e) => {
        console.log("你点击了这个图标；调用参数为：");
        console.log(e);
        this.showWindow(1)
      }
    };
  
    this.markerEvents2 = {
      click: (e) => {
        console.log("你点击了这个图标；调用参数为：");
        console.log(e);
        this.showWindow(2)
      }
    };

    this.markerEvents3 = {
      click: (e) => {
        console.log("你点击了这个图标；调用参数为：");
        console.log(e);
        this.showWindow(3)
      }
    };

    this.markerEvents4 = {
      click: (e) => {
        console.log("你点击了这个图标；调用参数为：");
        console.log(e);
        this.showWindow(4)
      }
    };

    this.markerEvents5 = {
      click: (e) => {
        console.log("你点击了这个图标；调用参数为：");
        console.log(e);
        this.showWindow(5)
      }
    };
  };
  showWindow(id) {
    this.setState({
      curVisibleWindow: id,
    });
  }
  
  closeWindow(){
    this.setState({
      curVisibleWindow: null,
    })
  }
  


  render() {
    const html1 = `<div><h3>Position 1</h3><p>PM2.5=256</p><p>PM10=70</p><p>温度：27.5</p><p>湿度：20%</p></div>`;
    const html2 = `<div><h3>Position 2</h3><p>PM2.5=152</p><p>PM10=60</p><p>温度：27</p><p>湿度：23%</p></div>`;
    const html3 = `<div><h3>Position 3</h3><p>PM2.5=106</p><p>PM10=60</p><p>温度：27</p><p>湿度：23%</p></div>`;
    const html4 = `<div><h3>Position 4</h3><p>PM2.5=62</p><p>PM10=60</p><p>温度：27</p><p>湿度：23%</p></div>`;
    const html5 = `<div><h3>Position 5</h3><p>PM2.5=240</p><p>PM10=60</p><p>温度：27</p><p>湿度：23%</p></div>`;
    
    const styleB = {
      backgroundImage: 'url('+iconTest+')',
      // backgroundColor: '#FFA500',
      // backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: '10px -296px',
      width: '35px',
      height: '26px',
      color: '#000',
      textAlign: 'center',
      lineHeight: '25px'
    }

    const styleC = {
      backgroundImage: 'url('+iconTest+')',
      // backgroundColor: '#FFA500',
      // backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: '10px -271px',
      width: '33px',
      height: '26px',
      color: '#000',
      textAlign: 'center',
      lineHeight: '25px'
    }
    
    return <div className="mapp-page" >
      <div style={styles.map}>
        <Map loading={Loading} amapkey={'eeb54f1e1bac5e5237b307344ad8e268'} version={'1.4.0'} zoom={7}>
          <Marker position={this.position1} clickable events={this.markerEvents1} ><div style={styleC}><b>256</b></div> </Marker>
          <Marker position={this.position2} clickable events={this.markerEvents2} ><div style={styleB}><b>132</b></div> </Marker>
          <Marker position={this.position3} clickable events={this.markerEvents3} ><div style={styleB}><b>106</b></div> </Marker>
          <Marker position={this.position4} clickable events={this.markerEvents4} /> 
          <Marker position={this.position5} clickable events={this.markerEvents5} ><div style={styleC}><b>240</b></div> </Marker>

          <InfoWindow
            position={this.position1}
            visible={this.state.curVisibleWindow === 1}
            content={html1}
            isCustom={false}
          />

          <InfoWindow
            position={this.position2}
            visible={this.state.curVisibleWindow === 2}
            content={html2}
            isCustom={false}
          />

          <InfoWindow
            position={this.position3}
            visible={this.state.curVisibleWindow === 3}
            content={html3}
            isCustom={false}
          />

          <InfoWindow
            position={this.position4}
            visible={this.state.curVisibleWindow === 4}
            content={html4}
            isCustom={false}
          />

          <InfoWindow
            position={this.position5}
            visible={this.state.curVisibleWindow === 5}
            content={html5}
            isCustom={false}
          />

          
          {/* <Markers 
            markers={this.state.markers} clickable events={this.markerEvents}
          /> */}
        </Map>
      </div>
      {/* <button onClick={() => { this.showWindow(4) }}>Show Window 4</button> */}
    </div>

  }
}
 const styles={
   map:{width: '100%', height: '600px', border: '2px solid'}
 }