import React, { Component } from 'react';
import DeviceListTable from './components/DeviceListTable';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';

export default class DeviceList extends Component {
  static displayName = 'DeviceList';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '设备管理', link: '' },
      { text: '设备列表', link: '#/version/list' },
    ];
    return (
      <div className="device-list-page">
        <CustomBreadcrumb dataSource={breadcrumb} />
        <DeviceListTable />
      </div>
    );
  }
}
