/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';
import { hashHistory } from "react-router";
import { Table, Input, Select, Grid, Card, Button, Range, Pagination  } from '@icedesign/base';
import { FormBinderWrapper, FormBinder } from '@icedesign/form-binder';
import IceContainer from '@icedesign/container';
import IceCard from '@icedesign/card';
// import SimpleFormDialog from '../SimpleFormDialog'

const { Combobox } = Select;
const { Row, Col } = Grid;

const dataSource = [
  {
    id: '1001',
    name: '雾霾盒子',
    devNumber: '2',
    status:'运行',
    address: '河南省开封市',
    time: '2018-01-28',
  },
  {
    id: '1002',
    name: '雾霾盒子',
    devNumber: '1',
    status:'运行',
    address: '河南省郑州市',
    time: '2018-04-25',
  },
  {
    id: '1003',
    name: '雾霾盒子',
    devNumber: '1',
    status:'停止',
    address: '河南省郑州市',
    time: '2018-05-20',
  },
  {
    id: '1007',
    name: '雾霾盒子4',
    devNumber: '1',
    status:'运行',
    address: '河南省郑州市',
    time: '2018-05-25',
  },
  {
    id: '1008',
    name: '雾霾盒子',
    devNumber: '1',
    status:'停止',
    address: '河南省郑州市',
    time: '2018-05-20',
  },
  {
    id: '1009',
    name: '雾霾盒子',
    devNumber: '1',
    status:'停止',
    address: '河南省郑州市',
    time: '2018-05-20',
  },
];

function handleChange(page) {
  hashHistory.push(page.toString());
}

export default class TagTable extends Component {
  static displayName = 'TagTable';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      formValue: dataSource,
    };
  }

  getDataSource = () => {
    const { formValue } = this.state;
    return dataSource.filter((data) => {
      // 预先筛除
      if (formValue.status && !data.status.match(formValue.status)) {
        return false;
      }

      if (formValue.id && !data.id.match(formValue.id)) {
        return false;
      }

      if (
        formValue.isHandle &&
        ((formValue.isHandle === 'YES' && !data.status) ||
          (formValue.isHandle === 'NO' && data.status))
      ) {
        return false;
      }

      if (
        formValue.levels &&
        !formValue.levels.some((l) => {
          return l === data.level;
        })
      ) {
        return false;
      }

      return true;
    });
  };

  onSort(dataIndex, order) {
    const dataSource = this.state.formValue.sort(function(a, b) {
      let result = a[dataIndex] - b[dataIndex];
      return order === "asc" ? (result > 0 ? 1 : -1) : result > 0 ? -1 : 1;
    });
    this.setState({
      dataSource
    });
  }

  formChange = (value) => {
    console.log('changed value', value);
    this.setState({
      formValue: value,
    });
  };

  detail = () => {
    window.location.href="#/device/deviceDetail";
  }

  renderOperator = (value, index, record) => {
    return (
        <Row>
          <Col>
            <Button onClick={this.detail} type="secondary">详情</Button>
          </Col>
          {/* <EditDialog
          index={index}
          record={record}
          getFormValues={this.getFormValues}
        />
        <DeleteBalloon
          handleRemove={() => this.handleRemove(value, index, record)}
        /> */}
        </Row>   
    );
  };

  render() {
    const { formValue } = this.state;

    return (
      <div className="tag-table">
        {/* <IceCard> */}
        <IceContainer>
          <FormBinderWrapper onChange={this.formChange}>
            <div style={{ marginBottom: '25px',borderBottom:'2px solid #D5D5D5' }}>
              <div style={styles.selectTitle}><span>按条件查询</span></div>
              <Row style={styles.formRow}>
                <Col xxs="6" s="4" l="2" style={styles.label}>
                  运行状态:{' '}
                </Col>
                <Col span="10">
                  <FormBinder>
                    <Input name="status"  />
                  </FormBinder>
                </Col>
                <Col xxs="6" s="4" l="2" style={styles.label} >
                  设备ID:{' '}
                </Col>
                <Col span="10">
                  <FormBinder>
                    <Input name="id" placeholder="请输入设备id" />
                  </FormBinder>
                </Col>
              </Row>
             
            </div>
          </FormBinderWrapper>

          {/* <Row>
            <SimpleFormDialog />
          </Row> */}

          <Table
            locale={{ empty: '没有查询到符合条件的记录' }}
            dataSource={this.getDataSource()}
            onSort={this.onSort.bind(this)}
          >          
            <Table.Column title="设备ID" align="center" dataIndex="id" width={80} sortable />
            <Table.Column title="设备名称" align="center" dataIndex="name" width={80} />
            <Table.Column title="状态" align="center" dataIndex="status" width={80} />
            <Table.Column title="地点" align="center" dataIndex="address" width={90} sortable/>
            <Table.Column title="注册时间" align="center" sortable dataIndex="time" width={80} />
            <Table.Column
              title="操作"
              align="center"
              cell={this.renderOperator}
              // lock="right"
              width={70}        
            />
          </Table>
        {/* </IceCard> */}

        <div style={{textAlign:'right',margin:'20px 10px'}}>
          <Pagination defaultCurrent={1} /> 
        </div>

        </IceContainer>
      </div>
    );
  }
}

const styles = {
  formRow: {
    // marginBottom: '25px',
    // marginLeft: '50px',
    margin: '15px 70px',
    fontSize: 16,
  },
  label: { lineHeight: '28px', paddingRight: '10px' },
  selectTitle: {
    fontSize:'20px',
    
  }
};