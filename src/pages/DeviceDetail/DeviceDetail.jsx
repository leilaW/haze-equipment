import React, { Component } from 'react';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import ChartBar from './components/ChartBar';
import ChartTypeLine from './components/ChartTypeLine';
import { Grid, Button, Notice, Icon, Card, Tab, Tag, Table, Switch } from '@icedesign/base';

const { Row, Col } = Grid;

const user = {
  name: 'sun',
  phone: '18638729321',
  email: '572220216@qq.com',
  address: '河南省郑州市金水区',
  uid: '1001',
  time1: '2018-05-06',
};

const data ={
  name: '雾霾盒子',
  id: '1001',
  statue: '运行',
  note: '雾霾盒子是真的好',
  dstate: '已绑定',
  time1: '2016-01-12'
}

export default class DeviceDetail extends Component {
  static displayName = 'DeviceDetail';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '设备管理', link: '' },
      { text: '设备列表', link: '#/version/list' },
      { text: '设备详情', link: '#/device/detail' },
    ];
    const styles = {
      basicDetailTitle: {
        margin: '10px 0',
        fontSize: '16px',
      },
      infoColumnDevice: {
        marginLeft: '16px',
      },
      infoColumnUser: {
        marginLeft: '16px',
        width:'360px',
      },
      infoColumnTitle: {
        margin: '20px 0',
        paddingLeft: '10px',
        borderLeft: '3px solid #3080fe',
        fontSize: 20
      },
      infoItems: {
        padding: 0,
        marginLeft: '25px',
      },
      infoItem: {
        marginBottom: '18px',
        listStyle: 'none',
        fontSize: '14px',
      },
      infoItemLabel: {
        minWidth: '70px',
        color: '#999',
      },
      infoItemValue: {
        color: '#333',
      },
      attachLabel: {
        minWidth: '70px',
        color: '#999',
        float: 'left',
      },
      attachPics: {
        width: '80px',
        height: '80px',
        border: '1px solid #eee',
        marginRight: '10px',
      },
      bigNum: {
        color: 'rgb(141,141,141)',
        // fontSize: 13,
      },
      symbol: {
        color: '#333',
        fontSize: 30,
        marginLeft: 10,
      },
      userRow: {
        margin:'7px 0'
      }

    };
    return (
      <div className="device-detail-page" >
        <CustomBreadcrumb dataSource={breadcrumb} />
      <Card style={{ width: '100%', borderRadius: '6px', marginBottom: '20px' }} bodyHeight='auto'>
        <Row><h2 style={{ color: '#000', paddingLeft: 15, fontWeight: 700, fontSize:21, marginBottom:15, marginTop:0 }}> <img width={30} src="http://112.74.168.74:8889/images/spec-detail-total/u2237.png" alt="" />  雾霾检测设备 </h2></Row>
        <div style={styles.infoColumnDevice}>
          <Row wrap style={styles.infoItems}>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>设备名称：</span>
              <span style={styles.infoItemValue}>{data.name}</span>
            </Col> 
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>设备ID：</span>
              <span style={styles.infoItemValue}>{data.id}</span>
            </Col>           
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>注册时间：</span>
              <span style={styles.infoItemValue}>{data.time1}</span>
            </Col>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>运行状态：</span>
              <span style={styles.infoItemValue}>{data.statue}</span>
            </Col>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>设备备注：</span>
              <span style={styles.infoItemValue}>{data.note}</span>
            </Col>
          </Row>
        </div>
      </Card>
      
            
              <Card bodyHeight="auto" style={{ width: '100%', borderRadius: '6px', marginBottom: '15px' }}>
              <Row>
                <Col fixedSpan="17" style={{borderRight:'2px solid #eee',marginRight:'40px'}}>
                  <h2 style={styles.basicDetailTitle}>用户信息</h2>
                  <div style={styles.infoColumn}>
                  <h5 style={styles.infoColumnTitle}>基本信息</h5>
                  <Row wrap style={styles.infoItems}>
                    <Col style={styles.infoItem}>
                      <Row style={styles.userRow}>
                        <span style={styles.infoItemLabel}>用户姓名：</span>
                        <span style={styles.infoItemValue}>{user.name}</span>
                      </Row>
                      <Row style={styles.userRow}>
                        <span style={styles.infoItemLabel}>用户编号：</span>
                        <span style={styles.infoItemValue}>{user.uid}</span>
                      </Row>
                      <Row style={styles.userRow}>
                        <span style={styles.infoItemLabel}>联系电话：</span>
                        <span style={styles.infoItemValue}>{user.phone} </span>
                      </Row>
                      <Row style={styles.userRow}>
                        <span style={styles.infoItemLabel}>联系地址：</span>
                        <span style={styles.infoItemValue}>{user.address}</span>
                      </Row>
                   
                    </Col>
                  </Row>
                  </div>
                </Col>
                <Col>
                  <h2 style={styles.basicDetailTitle}>设备介绍</h2>
                  <div style={styles.infoColumn}>
                  <h5 style={styles.infoColumnTitle}>设备规格</h5>
                  
                      <Row>
                      <Col fixedSpan="18">
                        <div style={{ backgroundColor: 'rgb(246,246,246)', height: '200px',width:'320px' }}>  
                        <img
                          style={{ width:'100%',background:'#333' }}
                          src="#"
                          alt=""
                        />
                        </div>
                      </Col>
                      <Col fixedSpan="16">
                        <Row>宽:10cm</Row>
                        <Row>高:10cm</Row>
                        <Row>重:10cm</Row>
                        <Row>设备描述：能够实时测量所处环境的PM2.5、PM10、温度、湿度的数值，并且可将数据上传至云端。平台能够通过查看云端数据，获得该设备的情况</Row>
                          {/* <Col>宽:10cm</Col>
                          <Col>高:10cm</Col>
                          <Col>重:10cm</Col>
                          <Col>设备描述：能够实时测量所处环境的PM2.5、PM10、温度、湿度的数值，并且可将数据上传至云端。平台能够通过查看云端数据，获得该设备的情况</Col> */}

                        
                        
                      </Col>
                      </Row>
                    </div>
                  </Col>
                  
                  
              </Row>

              </Card>
            
              {/* <MainData /> */}
              <Card bodyHeight="auto" style={{ width: '100%', borderRadius: '6px', marginBottom: '15px' }}>
                <Row guntter={20} style={{ height: 380, marginTop: '20px' }}>
                <Col l={12} s={24} xxs={24}>
                  <Card style={{ width: '98%', borderRadius: '6px' }} bodyHeight='auto'>
                    <ChartTypeLine />
                  </Card>
                </Col>
                <Col l={12} s={24} xxs={24}>
                  <Card style={{ width: '98%', float: 'right', borderRadius: '6px' }} bodyHeight='auto'>
                    <ChartBar />
                  </Card>
                </Col>
              </Row>
              </Card>    
            
         
      </div>
    );
  }
}
