import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Chart, Axis, Geom, Tooltip, Legend } from 'bizcharts';
import { DataSet } from '@antv/data-set';

export default class ChartBar extends Component {
  static displayName = 'ChartBar';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const data = [
      {
        name: '温度',
        'Jan.': 2,
        'Feb.': 9,
        'Mar.': 15,
        'Apr.': 22,
        May: 27.5,
        'Jun.': 30.4,
        'Jul.': 34,
        'Aug.': 38.7,
      },
      {
        name: '湿度',
        'Jan.': 12.4,
        'Feb.': 23.2,
        'Mar.': 34.5,
        'Apr.': 37.7,
        May: 39.6,
        'Jun.': 25.5,
        'Jul.': 29.4,
        'Aug.': 38.4,
      },
    ];

    const ds = new DataSet();
    const dv = ds.createView().source(data);
    dv.transform({
      type: 'fold',
      fields: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.', 'Aug.'], // 展开字段集
      key: '月份', // key字段
      value: '月均温度和湿度', // value字段
    });

    return (
      <div className="chart-bar">
        <IceContainer>
          <h4 style={styles.title}>月均温度和湿度</h4>
          <Chart height={300} data={dv} forceFit>
            <Axis name="月份" />
            <Axis name="月均温度和湿度" />
            <Legend />
            <Tooltip crosshairs={{ type: 'y' }} />
            <Geom
              type="interval"
              position="月份*月均温度和湿度"
              color="name"
              adjust={[{ type: 'dodge', marginRatio: 1 / 32 }]}
            />
          </Chart>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  title: {
    margin: '0 0 20px',
    fontSize: '18px',
    paddingBottom: '15px',
    fontWeight: 'bold',
    borderBottom: '1px solid #eee',
  },
};
