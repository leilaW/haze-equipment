import React, { Component } from 'react';
import { Chart, Axis, Geom, Tooltip } from 'bizcharts';
import { DataSet } from '@antv/data-set';
import IceContainer from '@icedesign/container';

export default class ChartTypeLine extends Component {
  static displayName = 'ChartTypeLine';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // 参考：https://alibaba.github.io/BizCharts/
    // 数据源
    const data = [
      { month: 'Jan', PM25: 170, PM10: 149 },
      { month: 'Feb', PM25: 99, PM10: 142 },
      { month: 'Mar', PM25: 95, PM10: 57 },
      { month: 'Apr', PM25: 145, PM10: 85 },
      { month: 'May', PM25: 104, PM10: 119 },
      { month: 'Jun', PM25: 85, PM10: 62 },
      { month: 'Jul', PM25: 62, PM10: 70 },
      { month: 'Aug', PM25: 25, PM10: 46 },
      { month: 'Sep', PM25: 63, PM10: 42 },
      { month: 'Oct', PM25: 83, PM10: 103 },
      { month: 'Nov', PM25: 139, PM10: 66 },
      { month: 'Dec', PM25: 96, PM10: 148 },
    ];

    // DataSet https://github.com/alibaba/BizCharts/blob/master/doc/tutorial/dataset.md#dataset
    const ds = new DataSet();
    const dv = ds.createView().source(data);
    dv.transform({
      type: 'fold',
      fields: ['PM25', 'PM10'],
      key: 'city',
      value: 'temperature',
    });

    // 定义度量
    const cols = {
      month: {
        range: [0, 1],
      },
    };

    return (
      <div className="chart-type-line">
        <IceContainer>
          <h4 style={styles.title}>PM2.5和PM10数值</h4>
          <Chart height={300} data={dv} scale={cols} forceFit>
            <Axis name="month" />
            <Axis
              name="temperature"
              label={{ formatter: (val) => `${val}` }}
            />
            <Tooltip crosshairs={{ type: 'y' }} />
            <Geom
              type="line"
              position="month*temperature"
              size={2}
              color="city"
              shape="smooth"
            />
            <Geom
              type="point"
              position="month*temperature"
              size={4}
              shape="circle"
              color="city"
              style={{ stroke: '#fff', lineWidth: 1 }}
            />
          </Chart>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  title: {
    margin: '0 0 40px',
    fontSize: '18px',
    paddingBottom: '15px',
    fontWeight: 'bold',
    borderBottom: '1px solid #eee',
  },
};
