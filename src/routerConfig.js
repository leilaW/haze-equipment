// 以下文件格式为描述路由的协议格式
// 你可以调整 routerConfig 里的内容
// 变量名 routerConfig 为 iceworks 检测关键字，请不要修改名称

import AsideLayout from './layouts/AsideLayout';
import Home from './pages/Home';
import Mapp from './pages/Mapp';
import UserList from './pages/UserList';
import UserDetail from './pages/UserDetail';
import DeviceList from './pages/DeviceList';
import DeviceDetail from './pages/DeviceDetail';
import LoginPage from './pages/LoginPage';
import BlankLayout from './layouts/BlankLayout';
import NotFound from './pages/NotFound';

const routerConfig = [
  {
    path: '/',
    layout: AsideLayout,
    component: Home,
  },
  {
    path: '/mapp',
    layout: AsideLayout,
    component: Mapp,
  },
  {
    path: '/user/userList',
    layout: AsideLayout,
    component: UserList,
  },
  {
    path: '/user/userDetail',
    layout: AsideLayout,
    component: UserDetail,
  },
  {
    path: '/device/DeviceList',
    layout: AsideLayout,
    component: DeviceList,
  },
  {
    path: '/device/DeviceDetail',
    layout: AsideLayout,
    component: DeviceDetail,
  },
  {
    path: '/LoginPage',
    layout: BlankLayout,
    component: LoginPage,
  },
  {
    path: '*',
    layout: AsideLayout,
    component: NotFound,
  },
];

export default routerConfig;
